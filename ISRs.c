/*
 * ISRs.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   Interrupt Service Routines
//

#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes
#include "macros.h"            // Include macro definitions
#include "GlobalVariables.h"   // Include Global Variable declarations

// Interrupt routines uses in this example:
__interrupt void epwm1_ISR(void)
{
    Int32 del;

    // Update freq to reach freq_set. Update is limited by slew rate (F_STEP)
    if (freq < freq_set)
    {
        freq_state += F_STEP;
    }
    if (freq > freq_set)
    {
        freq_state -= F_STEP;
    }
    freq = freq_state >> 16;

    // update theta from freq
    del = (Int32)freq*K_THETA;
    thetaState += del;
    theta = (Uint16)(thetaState>>22);
    theta90 = (theta+deg90)&0x3FF;

    // Read sinTheta and cosTheta from sine table
    sinTheta = sine[theta];
    cosTheta = sine[theta90];

    // V by f
    Vd = Qmul(abs(freq),K_VBYF,12+8-6);
    if (Vd < Q6(VD_MIN))      // Ensure minimum voltage
    {
        Vd = Q6(VD_MIN);
    }
    Vq = 0;                   // Vq is not used

    // D,Q to Al,Be
    Valfa = Qwx_yz(Vd,cosTheta,Vq,-sinTheta,15);
    Vbeta = Qwx_yz(Vd,sinTheta,Vq, cosTheta,15);

    // Al,Be to a,b,c
    Va = Valfa;
    Vb = Qwx_yz(Valfa,-Q15half,Vbeta, Q15sqrt3by2,15);
    Vc = Qwx_yz(Valfa,-Q15half,Vbeta,-Q15sqrt3by2,15);

    // Timing from Voltages
    Ta = QxyBYz(Va,PERIOD,Vdc);
    Tb = QxyBYz(Vb,PERIOD,Vdc);
    Tc = QxyBYz(Vc,PERIOD,Vdc);

    // Find Toffset
    Tmax = (  Ta>Tb) ?   Ta : Tb;
    Tmax = (Tmax>Tc) ? Tmax : Tc;
    Tmin = (  Ta<Tb) ?   Ta : Tb;
    Tmin = (Tmin<Tc) ? Tmin : Tc;
    Teff = Tmax - Tmin;
    Tzero = PERIOD - Teff;
    switch (pwm)
    {
        case SINE   :
            Toffset = PERIOD>>1;
            break;
        case SVPWM  :
            Toffset = (Tzero>>1) - Tmin;
            break;
        case LCLAMP :
            Toffset = -Tmin;
            break;
        case HCLAMP :
            Toffset = PERIOD - Tmax;
            break;
    }

    // Add offset to Timings
    Tao = Ta + Toffset;
    Tbo = Tb + Toffset;
    Tco = Tc + Toffset;

    EPwm1Regs.CMPA.half.CMPA = Tao;
    EPwm2Regs.CMPA.half.CMPA = Tbo;
    EPwm3Regs.CMPA.half.CMPA = Tco;

    // Va = sine[EPwm1TimerIntCount]/2+16384;
    // Ta = (Int16)(((Int32)Va*2500L)>>15);
    EPwm7Regs.CMPA.half.CMPA = Tao;
    EPwm7Regs.CMPB = Tbo;

    // Clear INT flag for this timer
    EPwm1Regs.ETCLR.bit.INT = 1;

    // Acknowledge this interrupt to receive more interrupts from group 3
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}

// Default ISR for unused interrupts
__interrupt void default_ISR(void)
{
    // Trap non initialized interrupts
    while(1);
}

// ISR for reserved interrupts
__interrupt void rsvd_ISR(void)
{
    // Trap reserved interrupts
    __asm ("      ESTOP0");
    while(1);
}

// ISR for reserved space in PIE vector table
__interrupt void PIE_RESERVED(void)
{
    __asm ("      ESTOP0");
    for(;;);
}
