/*
 * macros.h
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */

#ifndef MACROS_H_
#define MACROS_H_

// Functions type Macros
#define Qn(x,n) (Int16)((x)*(double)(2^(n)))
#define Q15(x)  (Int16)((x)*32768.0L)
#define Q12(x)  (Int16)((x)*4096.0L)
#define Q8(x)   (Int16)((x)*256.0L)
#define Q6(x)   (Int16)((x)*64.0L)
#define Qmul(x,y,n) (Int16)(((Int32)(x)*(Int32)(y))>>(n))
#define Qwx_yz(w,x,y,z,n) (Int16)(((Int32)(w)*(Int32)(x)+(Int32)(y)*(Int32)(z))>>(n))
#define QxyBYz(x,y,z) (Int16)(((Int32)(x)*(Int32)(y))/(Int32)(z))
//#define QxyBYz(x,y,z,n) (Int16)((((Int32)(x)*(Int32)(y))/(Int32)(z))>>n)

// Floating point values. Has to be converted to fp format before use.
#define FS          10000.0
#define TS          1.0/FS
#define VDC         200.0
#define VD_MIN      10.0
#define F_RATED     50.0

// Constants
#define deg90        256L  // 90deg = 1024/4
#define K_THETA      1677L // 1024*Ts*2^(22-Qf)
#define K_VBYF       Q12((VDC*0.5)/F_RATED)
#define Q15half      Q15(0.5)
#define Q15sqrt3by2  Q15(1.732051/2.0)
#define F_STEP       1024

// Constants used for PWM generation
#define RAMP   0

#if RAMP
#define PERIOD 9000   // fs = 10000 Hz, up count (RAMP Carrier)
#else                 // not RAMP, Traingular carrier
#define PERIOD 4500   // fs = 10000 Hz, up-down count
#endif // RAMP

#endif /* MACROS_H_ */
