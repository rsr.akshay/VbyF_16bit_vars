/*
 * Interrupt.c
 *
 * Author    : Najath Abdul Azeez
 * Copyright : http://opelex.net
 * License   : See LICENSE file
 *
 */
//
// TITLE:   Interrupts Initialization & Support Functions.
//

#include "F2806x_Device.h"     // Include F2806x Header files
#include "functions.h"         // Include function prototypes

const struct PIE_VECT_TABLE PieVectTableInit = {

    PIE_RESERVED,  // 1  Reserved space
    PIE_RESERVED,  // 2  Reserved space
    PIE_RESERVED,  // 3  Reserved space
    PIE_RESERVED,  // 4  Reserved space
    PIE_RESERVED,  // 5  Reserved space
    PIE_RESERVED,  // 6  Reserved space
    PIE_RESERVED,  // 7  Reserved space
    PIE_RESERVED,  // 8  Reserved space
    PIE_RESERVED,  // 9  Reserved space
    PIE_RESERVED,  // 10 Reserved space
    PIE_RESERVED,  // 11 Reserved space
    PIE_RESERVED,  // 12 Reserved space
    PIE_RESERVED,  // 13 Reserved space

    // Non-Peripheral Interrupts
    default_ISR,   // CPU-Timer 1
    default_ISR,   // CPU-Timer 2
    default_ISR,   // Datalogging interrupt
    default_ISR,   // RTOS interrupt
    default_ISR,   // Emulation interrupt
    default_ISR,   // Non-maskable interrupt
    default_ISR,   // Illegal operation TRAP
    default_ISR,   // User Defined trap 1
    default_ISR,   // User Defined trap 2
    default_ISR,   // User Defined trap 3
    default_ISR,   // User Defined trap 4
    default_ISR,   // User Defined trap 5
    default_ISR,   // User Defined trap 6
    default_ISR,   // User Defined trap 7
    default_ISR,   // User Defined trap 8
    default_ISR,   // User Defined trap 9
    default_ISR,   // User Defined trap 10
    default_ISR,   // User Defined trap 11
    default_ISR,   // User Defined trap 12

    // Group 1 PIE Vectors
    default_ISR,     // 1.1 ADC  ADC - make rsvd1_1 if ADCINT1 is wanted in Group 10 instead.
    default_ISR,     // 1.2 ADC  ADC - make rsvd1_2 if ADCINT2 is wanted in Group 10 instead.
    rsvd_ISR,        // 1.3
    default_ISR,     // 1.4 External Interrupt
    default_ISR,     // 1.5 External Interrupt
    default_ISR,     // 1.6 ADC Interrupt 9
    default_ISR,     // 1.7 Timer 0
    default_ISR,     // 1.8 WD, Low Power

    // Group 2 PIE Vectors
    default_ISR,     // 2.1 EPWM-1 Trip Zone
    default_ISR,     // 2.2 EPWM-2 Trip Zone
    default_ISR,     // 2.3 EPWM-3 Trip Zone
    default_ISR,     // 2.4 EPWM-4 Trip Zone
    default_ISR,     // 2.5 EPWM-5 Trip Zone
    default_ISR,     // 2.6 EPWM-6 Trip Zone
    default_ISR,     // 2.7 EPWM-7 Trip Zone
    default_ISR,     // 2.8 EPWM-8 Trip Zone

    // Group 3 PIE Vectors
    epwm1_ISR,       // 3.1 EPWM-1 Interrupt
    default_ISR,     // 3.2 EPWM-2 Interrupt
    default_ISR,     // 3.3 EPWM-3 Interrupt
    default_ISR,     // 3.4 EPWM-4 Interrupt
    default_ISR,     // 3.5 EPWM-5 Interrupt
    default_ISR,     // 3.6 EPWM-6 Interrupt
    default_ISR,     // 3.7 EPWM-7 Interrupt
    default_ISR,     // 3.8 EPWM-8 Interrupt

    // Group 4 PIE Vectors
    default_ISR,     // 4.1 ECAP-1
    default_ISR,     // 4.2 ECAP-2
    default_ISR,     // 4.3 ECAP-3
    rsvd_ISR,        // 4.4
    rsvd_ISR,        // 4.5
    rsvd_ISR,        // 4.6
    default_ISR,     // 4.7 HRCAP-1
    default_ISR,     // 4.8 HRCAP-2

    // Group 5 PIE Vectors

    default_ISR,     // 5.1 EQEP-1
    default_ISR,     // 5.2 EQEP-2
    rsvd_ISR,        // 5.3
    default_ISR,     // 5.4 HRCAP-3
    default_ISR,     // 5.5 HRCAP-4
    rsvd_ISR,        // 5.6
    rsvd_ISR,        // 5.7
    default_ISR,     // 5.8 USB-0

    // Group 6 PIE Vectors
    default_ISR,     // 6.1 SPI-A
    default_ISR,     // 6.2 SPI-A
    default_ISR,     // 6.3 SPI-B
    default_ISR,     // 6.4 SPI-B
    default_ISR,     // 6.5 McBSP-A
    default_ISR,     // 6.6 McBSP-A
    rsvd_ISR,        // 6.7
    rsvd_ISR,        // 6.8

    // Group 7 PIE Vectors
    default_ISR,     // 7.1 DMA Channel 1
    default_ISR,     // 7.2 DMA Channel 2
    default_ISR,     // 7.3 DMA Channel 3
    default_ISR,     // 7.4 DMA Channel 4
    default_ISR,     // 7.5 DMA Channel 5
    default_ISR,     // 7.6 DMA Channel 6
    rsvd_ISR,        // 7.7
    rsvd_ISR,        // 7.8

    // Group 8 PIE Vectors
    default_ISR,     // 8.1 I2C-A
    default_ISR,     // 8.2 I2C-A
    rsvd_ISR,        // 8.3
    rsvd_ISR,        // 8.4
    rsvd_ISR,        // 8.5
    rsvd_ISR,        // 8.6
    rsvd_ISR,        // 8.7
    rsvd_ISR,        // 8.8

    // Group 9 PIE Vectors
    default_ISR,     // 9.1 SCI-A
    default_ISR,     // 9.2 SCI-A
    default_ISR,     // 9.3 SCI-B
    default_ISR,     // 9.4 SCI-B
    default_ISR,     // 9.5 ECAN-A
    default_ISR,     // 9.6 ECAN-A
    rsvd_ISR,        // 9.7
    rsvd_ISR,        // 9.8

    // Group 10 PIE Vectors
    rsvd_ISR,        // 10.1 Can be ADCINT1, but must make ADCINT1 in Group 1 space "reserved".
    rsvd_ISR,        // 10.2 Can be ADCINT2, but must make ADCINT2 in Group 1 space "reserved".
    default_ISR,     // 10.3 ADC
    default_ISR,     // 10.4 ADC
    default_ISR,     // 10.5 ADC
    default_ISR,     // 10.6 ADC
    default_ISR,     // 10.7 ADC
    default_ISR,     // 10.8 ADC

    // Group 11 PIE Vectors
    default_ISR,     // 11.1 CLA1
    default_ISR,     // 11.2 CLA1
    default_ISR,     // 11.3 CLA1
    default_ISR,     // 11.4 CLA1
    default_ISR,     // 11.5 CLA1
    default_ISR,     // 11.6 CLA1
    default_ISR,     // 11.7 CLA1
    default_ISR,     // 11.8 CLA1

    // Group 12 PIE Vectors
    default_ISR,     // 12.1 External Interrupt
    rsvd_ISR,        // 12.2
    rsvd_ISR,        // 12.3
    rsvd_ISR,        // 12.4
    rsvd_ISR,        // 12.5
    rsvd_ISR,        // 12.6
    default_ISR,     // 12.7 Latched Overflow
    default_ISR      // 12.8 Latched Underflow
};

//---------------------------------------------------------------------------
// InitPieVectTable:
//---------------------------------------------------------------------------
// This function initializes the PIE vector table with appropriate ISR addresses.
// This function must be executed after boot time.
//
void InitPieVectTable(void)
{
    int16   i;
    Uint32 *Source = (void *) &PieVectTableInit;
    Uint32 *Dest = (void *) &PieVectTable;

    // Do not write over first 3 32-bit locations (these locations are
    // initialized by Boot ROM with boot variables)

    Source = Source + 3;
    Dest = Dest + 3;

    EALLOW;
    for(i=0; i < 125; i++)
        *Dest++ = *Source++;
    EDIS;

}

void InitInterrupts(void)
{

    // Disable Interrupts at the CPU level:
    DINT;

    // Disable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 0;

    // Clear all PIEIFR registers:
    PieCtrlRegs.PIEIFR1.all  = 0;
    PieCtrlRegs.PIEIFR2.all  = 0;
    PieCtrlRegs.PIEIFR3.all  = 0;
    PieCtrlRegs.PIEIFR4.all  = 0;
    PieCtrlRegs.PIEIFR5.all  = 0;
    PieCtrlRegs.PIEIFR6.all  = 0;
    PieCtrlRegs.PIEIFR7.all  = 0;
    PieCtrlRegs.PIEIFR8.all  = 0;
    PieCtrlRegs.PIEIFR9.all  = 0;
    PieCtrlRegs.PIEIFR10.all = 0;
    PieCtrlRegs.PIEIFR11.all = 0;
    PieCtrlRegs.PIEIFR12.all = 0;

    // Enable Used Interrupts. Clear all other PIEIER registers:
    PieCtrlRegs.PIEIER1.all  = 0;
    PieCtrlRegs.PIEIER2.all  = 0;
    PieCtrlRegs.PIEIER3.all  = 1; //Enable EPWM1 interrupt
    PieCtrlRegs.PIEIER4.all  = 0;
    PieCtrlRegs.PIEIER5.all  = 0;
    PieCtrlRegs.PIEIER6.all  = 0;
    PieCtrlRegs.PIEIER7.all  = 0;
    PieCtrlRegs.PIEIER8.all  = 0;
    PieCtrlRegs.PIEIER9.all  = 0;
    PieCtrlRegs.PIEIER10.all = 0;
    PieCtrlRegs.PIEIER11.all = 0;
    PieCtrlRegs.PIEIER12.all = 0;

    // Disable CPU interrupts and clear all CPU interrupt flags:
    IER = 0x0000;
    IFR = 0x0000;

    // Initialize the PIE vector table
    InitPieVectTable();

    // Enable the PIE
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;

    // Acknowledge all interrupts in PIE.
    // Enables PIE to drive a pulse into the CPU
    PieCtrlRegs.PIEACK.all = 0xFFFF;

    // Enable CPU INTx lines used in this project
    IER |= M_INT3; // INT3 which is connected to EPWM1-6 INT:

}

